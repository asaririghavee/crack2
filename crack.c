#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings
//char word[50];           

char *crackHash(char *target, char *dictionary)
{
     // Open the dictionary file
   FILE *psw = fopen(dictionary,"r");
     //check for error
   if(psw == NULL)
   {
      perror("Can't open dictionary");
      exit(1);
   }
     // Loop through the dictionary file, one line
    // at a time.
    char *word = (char *)malloc(sizeof(char)*PASS_LEN);
   while(fgets(word,PASS_LEN,psw) != NULL)
   {  
       
      char *nl = strchr(word, '\n');
      if(nl != NULL)
         *nl = '\0';
         // Hash each password. Compare to the target hash.
    // If they match, return the corresponding password.
      char *hash = md5(word, strlen(word));
      if (strcmp(hash,target) == 0)
      {
      // Free up memory?
         free(hash);
         fclose (psw);
         return word;
      }
   }
   return NULL;
}

int main(int argc, char *argv[])
{
   char pass[HASH_LEN+1];
   char hashlistfilename[20]="";
   char dictionaryname[20]="";
    //read the dictionary name and hash file name from console
   if (argc < 3) 
   {
      printf("Usage: %s hash_file dict_file\n", argv[0]);
      exit(1);
   }
   strcpy(hashlistfilename,argv[1]);
   strcpy(dictionaryname,argv[2]);
    
    // Open the hash file for reading.
   FILE *hash = fopen(hashlistfilename,"r");
    //check for error
   if(hash == NULL)
   {
      perror("Can't open dictionary");
      exit(1);
   }
    
   while(fgets(pass,HASH_LEN+1,hash) != NULL)
   {  
       //look for \n and change \n with \0 
      char *nl = strchr(pass, '\n');
      if(nl != NULL)
         *nl = '\0';
        // For each hash, crack it by passing it to crackHash
         // Display the hash along with the cracked password:
      printf("%s %s\n", pass,crackHash(pass,dictionaryname));
   }
    // Close the hash file
   fclose (hash);
    // Free up any malloc'd memory?
}